package com.example.lynn.myclock;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static com.example.lynn.myclock.MainActivity.*;

/**
 * Created by lynn on 6/15/2016.
 */
public class ClockView extends LinearLayout {

    public ClockView(Context context) {
        super(context);

        setBackground(getResources().getDrawable(R.drawable.background));

        buttons = new Button[9];

        for (int counter=0;counter<buttons.length;counter++) {
            buttons[counter] = new Button(context);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200,200);

            buttons[counter].setLayoutParams(layoutParams);

            buttons[counter].setText("0");

            buttons[counter].setTextSize(40);

            addView(buttons[counter]);
        }

        buttons[2].setText(":");
        buttons[5].setText(":");
        buttons[8].setText("pm");

        String[] ids = TimeZone.getAvailableIDs();

        List<String> list = new ArrayList<>();

        for (String id : ids)
            list.add(id);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        timeZones = new Spinner(context);

        timeZones.setAdapter(adapter);

        addView(timeZones);

        new MyThread();
    }

}
