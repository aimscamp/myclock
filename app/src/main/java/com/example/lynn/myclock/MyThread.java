package com.example.lynn.myclock;

import java.util.Calendar;
import java.util.TimeZone;

import static com.example.lynn.myclock.MainActivity.*;

/**
 * Created by lynn on 6/15/2016.
 */
public class MyThread implements Runnable {
    private Thread thread;
    private boolean keepGoing;

    public MyThread() {
        thread = new Thread(this);

        keepGoing = true;

        thread.start();
    }

    public void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    public void getTime() {
        Calendar c = Calendar.getInstance();

        TimeZone timeZone = TimeZone.getTimeZone(timeZones.getSelectedItem().toString());

        c.setTimeZone(timeZone);

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        final boolean am = hour < 12;

        hour -= (hour > 12) ? 12 : 0;

        final int hourFirstDigit = hour / 10;
        final int hourSecondDigit = hour % 10;

        final int minuteFirstDigit = minute / 10;
        final int minuteSecondDigit = minute % 10;

        final int secondFirstDigit = second / 10;
        final int secondSecondDigit = second % 10;

        buttons[0].post(new Runnable() {

            @Override
            public void run() {
                buttons[0].setText(String.valueOf(hourFirstDigit));
                buttons[1].setText(String.valueOf(hourSecondDigit));
                buttons[3].setText(String.valueOf(minuteFirstDigit));
                buttons[4].setText(String.valueOf(minuteSecondDigit));
                buttons[6].setText(String.valueOf(secondFirstDigit));
                buttons[7].setText(String.valueOf(secondSecondDigit));
                buttons[8].setText(String.valueOf((am) ? "am" : "pm"));
            }

        });



    }


    @Override
    public void run() {
       while (keepGoing) {
           getTime();

           pause(0.5);
       }
    }

}
